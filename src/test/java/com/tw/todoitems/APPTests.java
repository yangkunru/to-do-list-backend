package com.tw.todoitems;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.tw.todoitems.model.Item;
import com.tw.todoitems.model.ItemStatus;
import com.tw.todoitems.service.ToDoItems;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.equalTo;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class APPTests {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private ToDoItems toDoItems;

    private ObjectMapper objectMapper = new ObjectMapper();

    @Test
    void contextLoads() {
    }

    @Test
    void should_get_all_items() throws Exception {
        List<Item> itemList = Arrays.asList(new Item(1, "Learning Java", ItemStatus.ACTIVE), new Item(2, "Learing JS", ItemStatus.COMPLETED));
        given(toDoItems.getAllItems()).willReturn(itemList);
        this.mvc.perform(get("/items").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(equalTo("[{\"id\":1,\"text\":\"Learning Java\",\"status\":\"ACTIVE\"},{\"id\":2,\"text\":\"Learing JS\",\"status\":\"COMPLETED\"}]")));
    }

    @Test
    void  should_return_item_when_create_item() throws Exception {
        given(toDoItems.createItem(any(Item.class))).willReturn(new Item(1, "Learning Java", ItemStatus.ACTIVE));

        this.mvc.perform(MockMvcRequestBuilders.post("/items").content(objectMapper.writeValueAsString(new Item("Learing Java"))).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(equalTo("{\"id\":1,\"text\":\"Learning Java\",\"status\":\"ACTIVE\"}")));
    }

    @Test
    void  should_return_true_when_update_item() throws Exception {
        given(toDoItems.updateItem(any(Item.class))).willReturn(true);

        this.mvc.perform(MockMvcRequestBuilders.put("/items").content(objectMapper.writeValueAsString(new Item(1,"Learing Java",ItemStatus.COMPLETED))).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(equalTo("true")));
    }

    @Test
    void should_return_status_ok_when_delete_item() throws Exception {
        given(toDoItems.deleteItem(anyInt())).willReturn(true);

        this.mvc.perform(MockMvcRequestBuilders.delete("/items/1").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }
}
