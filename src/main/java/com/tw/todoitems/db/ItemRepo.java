package com.tw.todoitems.db;

import com.tw.todoitems.model.Item;
import com.tw.todoitems.model.ItemStatus;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ItemRepo {
    private Connector connector;

    public ItemRepo() {
        this.connector = new Connector();
    }

    public Item addItem(Item item) {
        // Need to be implemented
        String sql = String.format("INSERT INTO items (text, status) VALUES ('%s','%s');", item.getText(), item.getStatus().toString());
        try (Connection connection = connector.createConnect();
            PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.executeUpdate();
            ResultSet resultSet = preparedStatement.getGeneratedKeys();
            while(resultSet.next()) {
                return findItems().get(findItems().size() - 1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Item> findItems() {
        // Need to be implemented
        List<Item> itemList = new ArrayList<Item>();
        String sql = "SELECT * FROM items";
        try (Connection connection = connector.createConnect();
        Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(sql);
            while(resultSet.next()) {
                int id = resultSet.getInt("id");
                String text = resultSet.getString("text");
                String status = resultSet.getString("status");

                Item item = new Item(id,text, ItemStatus.valueOf(status));
                itemList.add(item);
            }
            return itemList;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean updateItem(Item item) {
        // Need to be implemented
        String sql = String.format("UPDATE items SET text = '%s', status = '%s' WHERE id = %d", item.getText(), item.getStatus(), item.getId());
        try (Connection connection = connector.createConnect();
             PreparedStatement preparedStatement = connection.prepareStatement(sql);
        ) {
            return preparedStatement.executeUpdate() > 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean deleteItem(int id) {
        // Need to be implemented
        String sql = String.format("DELETE FROM items WHERE id = %d", id);
        try (Connection connection = connector.createConnect();
        PreparedStatement preparedStatement = connection.prepareStatement(sql);) {
            return preparedStatement.executeUpdate() > 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

}
